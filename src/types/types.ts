export interface Meal {
  id: number;
  name: string;
  price: number;
}

export interface MealMenu {
  [title: string]: Meal[];
}

export interface MealWithQuantity extends Meal {
  selected: number;
}

export interface MealWithQuantityIndex {
  [id: number]: MealWithQuantity;
}
