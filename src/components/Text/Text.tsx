import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles<Theme, TextProps>(() =>
  createStyles({
    root: {
      fontSize: ({ fontSize = 14 }) => fontSize,
      color: ({ color }) => color,
      fontWeight: ({ fontWeight }) => fontWeight,
    },
  })
);

export interface TextProps {
  children: React.ReactNode;
  className?: string;
  fontSize?: number;
  color?: string;
  fontWeight?: number;
}

export const Text: React.FC<TextProps> = (props) => {
  const { children, className } = props;
  const classes = useStyles(props);
  return <span className={clsx(className, classes.root)}>{children}</span>;
};
