import React from 'react';
import { Story, Meta } from '@storybook/react';
import { MealCard, MealCardProps } from './MealCard';

export default {
  title: 'Molecules/MealCard',
  component: MealCard,
} as Meta;

const Template: Story<MealCardProps> = (args) => <MealCard {...args} />;

export const Default = Template.bind({});
Default.args = {
  item: {
    id: 1,
    name: 'Soup',
    price: 3,
  },
  selectMeal: () => {},
  deselectMeal: () => {},
  selected: false,
};

export const Selected = Template.bind({});
Selected.args = {
  item: {
    id: 1,
    name: 'Soup',
    price: 3,
  },
  selectMeal: () => {},
  deselectMeal: () => {},
  selected: true,
};
