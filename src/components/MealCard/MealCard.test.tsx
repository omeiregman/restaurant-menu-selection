import { render, screen, cleanup } from '@testing-library/react';
import { MealCard } from './MealCard';

afterEach(() => {
  cleanup();
});

describe('MealCard', () => {
  it('should render meal card component', () => {
    render(
      <MealCard
        item={{
          id: 1,
          name: 'Soup',
          price: 3,
        }}
        selectMeal={jest.fn()}
        deselectMeal={jest.fn()}
        selected={false}
      />
    );
    const MealCardComponent = screen.getByTestId('meal-card');
    expect(MealCardComponent).toBeInTheDocument();
    expect(MealCardComponent).toHaveTextContent('Soup');
  });
});
