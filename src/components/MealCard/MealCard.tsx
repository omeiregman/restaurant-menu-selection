import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import clsx from 'clsx';
import { Meal } from '../../types/types';
import { Flex } from '../Flex/Flex';
import { Card } from '../Card/Card';
import { Text } from '../Text/Text';
import { parseRawPrice } from '../../utils/utils';
import { Button } from '../Button/Button';

const useStyles = makeStyles<Theme>((theme) =>
  createStyles({
    root: {
      padding: 0,
    },
    content: {
      justifyContent: 'space-between',
    },
    selectBorder: {
      border: `1px solid ${theme.palette.primary.main}`,
    },
  })
);

export interface MealCardProps {
  item: Meal;
  selectMeal: (meal: Meal) => void;
  deselectMeal: (meal: Meal) => void;
  selected: boolean;
  className?: string;
}

export const MealCard: React.FC<MealCardProps> = ({
  item,
  selectMeal,
  deselectMeal,
  selected,
  className,
}) => {
  const classes = useStyles();

  const toggleSelected = () => {
    if (!selected) {
      selectMeal(item);
    } else {
      deselectMeal(item);
    }
  };

  return (
    <Card
      width={200}
      height={90}
      className={clsx(className, classes.root, {
        [classes.selectBorder]: selected,
      })}
      testId="meal-card"
    >
      <Button
        label="meal"
        styleType="transparent"
        type="button"
        onClick={toggleSelected}
        width="100%"
        height="100%"
      >
        <Flex className={classes.content}>
          <Text fontSize={14} fontWeight={500}>
            {item.name}
          </Text>
          <Text>{parseRawPrice(item.price)}</Text>
        </Flex>
      </Button>
    </Card>
  );
};
