import { render, screen, cleanup } from '@testing-library/react';
import { MealsSelectionSection } from './MealsSelectionSection';
import { menuData } from '../../utils/mock';

afterEach(() => {
  cleanup();
});

describe('MealSelection', () => {
  it('should render meal course component', () => {
    render(
      <MealsSelectionSection
        menu={menuData}
        addToSummary={jest.fn()}
        removeFromSummary={jest.fn()}
        setDisabled={jest.fn()}
      />
    );
    const MealsSelectionSectionComponent = screen.getByTestId(
      'meal-selection-section'
    );
    expect(MealsSelectionSectionComponent).toBeInTheDocument();
  });
});
