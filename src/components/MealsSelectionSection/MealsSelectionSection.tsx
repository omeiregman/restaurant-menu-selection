import React, { useCallback, useEffect, useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { Meal, MealMenu } from '../../types/types';
import { Flex } from '../Flex/Flex';
import { Text } from '../Text/Text';
import { MealCourse, SelectedMealById } from '../MealCourse/MealCourse';
import { config } from '../../config';

const useStyles = makeStyles<Theme>((theme) =>
  createStyles({
    root: {
      flexDirection: 'column',
      border: `1px solid ${theme.palette.primary.main}`,
      padding: theme.spacing(3),
      borderRadius: 8,
    },
    error: {
      color: theme.palette.error.main,
      marginTop: theme.spacing(1),
    },
  })
);

export interface MealsSelectionSectionProps {
  menu: MealMenu;
  addToSummary: (meal: Meal) => void;
  removeFromSummary: (meal: Meal) => void;
  setDisabled: (status: boolean) => void;
}

export const MealsSelectionSection: React.FC<MealsSelectionSectionProps> = ({
  menu,
  addToSummary,
  removeFromSummary,
  setDisabled,
}) => {
  const classes = useStyles();
  const [selectedMeals, setSelectedMeals] = useState<SelectedMealById>({});
  const [isMainSelected, setIsMainSelected] = useState(false);

  const getMealSelectionError = () => {
    const { mealRestrictionPair } = config;
    const mainMealError = !isMainSelected && 'Main Course must be in selection';
    const mealRestrictionError: string[] = [];
    mealRestrictionPair.forEach((item) => {
      if (!!selectedMeals[item[0]] && !!selectedMeals[item[1]]) {
        mealRestrictionError.push(
          `Meal ${selectedMeals[item[0]].name} and ${
            selectedMeals[item[1]].name
          } cannot be selected together`
        );
      }
    });
    if (mainMealError) mealRestrictionError.push(mainMealError);
    return mealRestrictionError;
  };

  const errors = getMealSelectionError();
  const selectedMealCount = Object.values(selectedMeals).length;

  const addMeal = (meal: Meal) => {
    const newSelected = { [meal.id]: meal };
    setSelectedMeals({ ...selectedMeals, ...newSelected });
    const { mains } = menu;
    if (mains.includes(meal)) setIsMainSelected(true);
    addToSummary(meal);
  };

  const removeMeal = (meal: Meal) => {
    const selected: SelectedMealById = { ...selectedMeals };
    delete selected[meal.id];
    setSelectedMeals({ ...selected });
    const { mains } = menu;
    if (mains.includes(meal)) setIsMainSelected(false);
    removeFromSummary(meal);
  };

  const setError = useCallback(
    () => selectedMealCount < config.minMealAmount || errors.length > 0,
    [errors.length, selectedMealCount]
  );

  useEffect(() => {
    setDisabled(setError());
  }, [setError, setDisabled, selectedMeals]);

  return (
    <Flex className={classes.root} testId="meal-selection-section">
      {Object.keys(menu).map((item) => (
        <MealCourse
          key={item}
          meals={menu[item]}
          title={item}
          addSelectedMeal={addMeal}
          removeSelectedMeal={removeMeal}
        />
      ))}
      {errors.map((error) => (
        <Text className={classes.error} key={error}>
          {error}
        </Text>
      ))}
    </Flex>
  );
};
