import React from 'react';
import { Story, Meta } from '@storybook/react';
import {
  MealsSelectionSection,
  MealsSelectionSectionProps,
} from './MealsSelectionSection';
import { menuData } from '../../utils/mock';

export default {
  title: 'Organisms/MealsSelectionSection',
  component: MealsSelectionSection,
} as Meta;

const Template: Story<MealsSelectionSectionProps> = (args) => (
  <MealsSelectionSection {...args} />
);

export const Default = Template.bind({});
Default.args = {
  menu: menuData,
  setDisabled: () => {},
  addToSummary: () => {},
  removeFromSummary: () => {},
};
