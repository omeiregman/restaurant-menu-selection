import React from 'react';
import { Story, Meta } from '@storybook/react';

import { Card, CardProps } from './Card';

export default {
  title: 'Atoms/Card',
  component: Card,
} as Meta;

const Template: Story<CardProps> = (args) => (
  <Card {...args}>Content Here</Card>
);

export const Primary = Template.bind({});
Primary.args = {
  width: 250,
  height: 100,
};
