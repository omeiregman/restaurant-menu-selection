import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { Flex } from '../../Flex/Flex';
import { Text } from '../../Text/Text';
import { MealsSelectionSection } from '../../MealsSelectionSection/MealsSelectionSection';
import { menuData } from '../../../utils/mock';
import { PriceSummary } from '../../PriceSummary/PriceSummary';
import { Meal, MealWithQuantityIndex } from '../../../types/types';
import { addSummaryItem, removeSummaryItem } from '../../../utils/utils';
import { Button } from '../../Button/Button';
import { config } from '../../../config';

const useStyles = makeStyles<Theme>((theme) =>
  createStyles({
    root: {
      flexDirection: 'row',
      padding: theme.spacing(3),
    },
    mealSectionTitle: {
      fontSize: 16,
      fontWeight: 500,
      marginBottom: theme.spacing(1),
    },
    selectionSection: {
      flexDirection: 'column',
    },
    selection: {
      flexDirection: 'column',
      marginBottom: theme.spacing(2),
    },
    summarySection: {
      flexDirection: 'column',
      alignItems: 'center',
      margin: theme.spacing(0, 'auto'),
    },
    error: {
      color: theme.palette.error.main,
    },
  })
);

export const Home: React.FC = () => {
  const classes = useStyles();
  const [totalPrice, setTotalPrice] = useState(0);
  const [priceSummary, setPriceSummary] = useState<MealWithQuantityIndex>({});
  const [isFirstTableDisabled, setIsFirstTableDisabled] = useState(true);
  const [isSecondTableDisabled, setIsSecondTableDisabled] = useState(true);

  const summary = Object.values(priceSummary);

  const validateLimitedStock = () => {
    const limitedItem = config.limitedQuantityMeals[0];
    if (priceSummary[limitedItem.id]?.selected > limitedItem.quantity) {
      return `Only one ${priceSummary[limitedItem.id].name} left`;
    }
    return '';
  };

  const limitedStockError = validateLimitedStock();

  const handleAddMeal = (meal: Meal) => {
    const newPriceSummary = addSummaryItem(meal, priceSummary, totalPrice);
    setTotalPrice(newPriceSummary.totalPrice);
    setPriceSummary({ ...priceSummary, ...newPriceSummary.summary });
  };

  const handleRemoveMeal = (meal: Meal) => {
    const newPriceSummary = removeSummaryItem(meal, priceSummary, totalPrice);
    setTotalPrice(newPriceSummary.totalPrice);
    setPriceSummary({ ...newPriceSummary.summary });
  };

  const getFirstTableStatus = (status: boolean) => {
    setIsFirstTableDisabled(status);
  };

  const getSecondTableStatus = (status: boolean) => {
    setIsSecondTableDisabled(status);
  };

  return (
    <Flex className={classes.root}>
      <Flex className={classes.selectionSection}>
        <Flex className={classes.selection}>
          <Text className={classes.mealSectionTitle}>
            Select meals for table one
          </Text>
          <MealsSelectionSection
            menu={menuData}
            addToSummary={handleAddMeal}
            removeFromSummary={handleRemoveMeal}
            setDisabled={getFirstTableStatus}
          />
        </Flex>
        <Flex className={classes.selection}>
          <Text className={classes.mealSectionTitle}>
            Select meals for Table two
          </Text>
          <MealsSelectionSection
            menu={menuData}
            addToSummary={handleAddMeal}
            removeFromSummary={handleRemoveMeal}
            setDisabled={getSecondTableStatus}
          />
        </Flex>
      </Flex>
      <Flex className={classes.summarySection}>
        {limitedStockError && (
          <Text className={classes.error}>{limitedStockError}</Text>
        )}
        <PriceSummary summary={summary} totalPrice={totalPrice} />
        <Button
          label="reserve"
          width="100%"
          disabled={
            isFirstTableDisabled ||
            isSecondTableDisabled ||
            Boolean(limitedStockError)
          }
        >
          Reserve for 2 Tables
        </Button>
      </Flex>
    </Flex>
  );
};
