import { render, screen, cleanup } from '@testing-library/react';
import { PriceSummary } from './PriceSummary';

afterEach(() => {
  cleanup();
});

describe('PriceSummary', () => {
  it('should render price summary component', () => {
    render(<PriceSummary summary={[]} totalPrice={12} />);
    const PriceSummaryComponent = screen.getByTestId('price-summary');
    expect(PriceSummaryComponent).toBeInTheDocument();
    expect(PriceSummaryComponent).toHaveTextContent('Total$12.00');
  });
});
