import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { Flex } from '../Flex/Flex';
import { Text } from '../Text/Text';
import { parseRawPrice } from '../../utils/utils';
import { MealWithQuantity } from '../../types/types';

const useStyles = makeStyles<Theme>((theme) =>
  createStyles({
    root: {
      flexDirection: 'column',
      padding: theme.spacing(1),
      width: 400,
    },
    summary: {
      justifyContent: 'space-between',
      marginBottom: theme.spacing(1),
    },
    total: {
      justifyContent: 'space-between',
      borderTop: `1px solid ${theme.palette.secondary.main}`,
      marginTop: theme.spacing(2),
      paddingTop: theme.spacing(2),
    },
  })
);

export interface PriceSummaryProps {
  summary: MealWithQuantity[];
  totalPrice: number;
}

export const PriceSummary: React.FC<PriceSummaryProps> = ({
  summary,
  totalPrice,
}) => {
  const classes = useStyles();
  return (
    <Flex className={classes.root} testId="price-summary">
      <Text fontWeight={500} fontSize={16}>
        Price Summary
      </Text>
      {summary.map((item) => (
        <Flex key={item.id} className={classes.summary}>
          <Text>{`${item.name} ${
            item.selected > 1 ? `(x ${item.selected})` : ''
          }`}</Text>
          <Text>{parseRawPrice(item.price)}</Text>
        </Flex>
      ))}
      <Flex className={classes.total}>
        <Text fontWeight={500}>Total</Text>
        <Text fontWeight={500}>{parseRawPrice(totalPrice)}</Text>
      </Flex>
    </Flex>
  );
};
