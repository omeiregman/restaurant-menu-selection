import React from 'react';
import { Story, Meta } from '@storybook/react';
import { PriceSummary, PriceSummaryProps } from './PriceSummary';

export default {
  title: 'Molecules/PriceSummary',
  component: PriceSummary,
} as Meta;

const Template: Story<PriceSummaryProps> = (args) => <PriceSummary {...args} />;

export const Default = Template.bind({});
Default.args = {
  summary: [],
  totalPrice: 230,
};
