import React from 'react';
import { Story, Meta } from '@storybook/react';
import { MealCourse, MealCourseProps } from './MealCourse';

export default {
  title: 'Molecules/MealCourse',
  component: MealCourse,
} as Meta;

const Template: Story<MealCourseProps> = (args) => <MealCourse {...args} />;

export const Default = Template.bind({});
Default.args = {
  meals: [
    {
      id: 5,
      name: 'Steak',
      price: 18,
    },
    {
      id: 6,
      name: 'Meatballs',
      price: 11.5,
    },
    {
      id: 7,
      name: 'Salmon fillet',
      price: 14,
    },
    {
      id: 8,
      name: 'Vegetarian lasagna',
      price: 12,
    },
  ],
  title: 'Main course',
  addSelectedMeal: () => {},
  removeSelectedMeal: () => {},
};
