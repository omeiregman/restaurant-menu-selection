import { render, screen, cleanup } from '@testing-library/react';
import { MealCourse } from './MealCourse';
import { menuData } from '../../utils/mock';

afterEach(() => {
  cleanup();
});

describe('MealCourse', () => {
  it('should render meal course component', () => {
    render(
      <MealCourse
        meals={menuData.starters}
        title="Starters"
        addSelectedMeal={jest.fn()}
        removeSelectedMeal={jest.fn()}
      />
    );
    const MealCourseComponent = screen.getByTestId('meal-course');
    expect(MealCourseComponent).toBeInTheDocument();
    expect(MealCourseComponent).toHaveTextContent('Starters');
  });
});
