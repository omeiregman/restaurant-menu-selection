import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { Meal } from '../../types/types';
import { Flex } from '../Flex/Flex';
import { Text } from '../Text/Text';
import { MealCard } from '../MealCard/MealCard';
import { config } from '../../config';

const useStyles = makeStyles<Theme>((theme) =>
  createStyles({
    root: {
      flexDirection: 'column',
    },
    mealTitle: {
      textTransform: 'uppercase',
      marginBottom: theme.spacing(1),
      fontSize: 16,
      fontWeight: 500,
    },
    mealsList: {
      flexWrap: 'wrap',
    },
    mealItem: {
      marginBottom: theme.spacing(2),
      '&:not(:last-child)': {
        marginRight: theme.spacing(2),
      },
    },
  })
);
interface MealWitQuantity extends Meal {
  selected?: number;
}

export interface SelectedMealById {
  [id: number]: MealWitQuantity;
}

export interface MealCourseProps {
  meals: Meal[];
  title: string;
  addSelectedMeal: (meal: Meal) => void;
  removeSelectedMeal: (meal: Meal) => void;
}

export const MealCourse: React.FC<MealCourseProps> = ({
  meals,
  title,
  addSelectedMeal,
  removeSelectedMeal,
}) => {
  const classes = useStyles();
  const [selectedMeal, setSelectedMeal] = useState<SelectedMealById>({});

  const isMealSelectionDisabled = () =>
    Object.keys(selectedMeal).length >= config.maxMealPerCourse;

  const selectMeal = (meal: Meal) => {
    if (isMealSelectionDisabled()) return;
    const newSelected = { [meal.id]: meal };
    setSelectedMeal({ ...selectedMeal, ...newSelected });
    addSelectedMeal(meal);
  };

  const deselectMeal = (meal: Meal) => {
    const selected = selectedMeal;
    delete selected[meal.id];
    setSelectedMeal({ ...selected });
    removeSelectedMeal(meal);
  };

  return (
    <Flex className={classes.root} testId="meal-course">
      <Text className={classes.mealTitle}>{title}</Text>
      <Flex className={classes.mealsList}>
        {meals.map((meal) => (
          <MealCard
            key={meal.id}
            item={meal}
            selected={Boolean(selectedMeal[meal.id])}
            selectMeal={() => selectMeal(meal)}
            deselectMeal={() => deselectMeal(meal)}
            className={classes.mealItem}
          />
        ))}
      </Flex>
    </Flex>
  );
};
