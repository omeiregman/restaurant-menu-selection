import { render, screen, cleanup, fireEvent } from '@testing-library/react';
import { Button } from './Button';

afterEach(() => {
  cleanup();
});

describe('Button Component test', () => {
  it('should render Button component', () => {
    render(
      <Button label="button" testId="button">
        Click Me
      </Button>
    );
    const ButtonComponent = screen.getByTestId('button');
    expect(ButtonComponent).toBeInTheDocument();
    expect(ButtonComponent).toHaveTextContent('Click Me');
  });

  it('calls onClick', () => {
    const handleClick = jest.fn();
    render(
      <Button label="button" testId="button" onClick={handleClick}>
        Click Me
      </Button>
    );
    const ButtonComponent = screen.getByTestId('button');
    fireEvent.click(ButtonComponent);
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
