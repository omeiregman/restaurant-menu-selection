import React, { ReactNode } from 'react';
import clsx from 'clsx';
import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles<Theme, ButtonProps>((theme) =>
  createStyles({
    root: {
      backgroundColor: ({ styleType }) =>
        styleType === 'transparent'
          ? 'transparent'
          : theme.palette.primary.main,
      borderRadius: 6,
      cursor: 'pointer',
      border: 0,
      height: ({ height = 48 }) => height,
      width: ({ width = 100 }) => width,
      fontSize: 16,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
      '&:hover': {
        backgroundColor: ({ styleType }) =>
          styleType === 'transparent'
            ? 'transparent'
            : theme.palette.primary.light,
      },
      '&:disabled': {
        cursor: 'not-allowed',
        backgroundColor: theme.palette.secondary.light,
        '&:hover': {
          backgroundColor: theme.palette.secondary.light,
        },
      },
    },
  })
);

export interface ButtonProps {
  children: ReactNode;
  type?: 'button' | 'submit';
  onClick?: () => void;
  className?: string;
  label: string;
  disabled?: boolean;
  width?: number | string;
  height?: number | string;
  styleType?: string;
  testId?: string;
}

export const Button: React.FC<ButtonProps> = (props) => {
  const { type, className, children, onClick, label, disabled, testId } = props;
  const classes = useStyles(props);
  return (
    <button
      type={type === 'submit' ? 'submit' : 'button'}
      className={clsx(classes.root, className)}
      onClick={onClick}
      data-aria-label-resource-key={label}
      aria-label={label}
      disabled={disabled}
      data-testid={testId}
    >
      {children}
    </button>
  );
};
