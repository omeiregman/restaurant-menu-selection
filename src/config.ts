export const config = {
  maxMealPerCourse: 1,
  minMealAmount: 2,
  mealRestrictionPair: [[4, 7]],
  limitedQuantityMeals: [{ id: 11, quantity: 1 }],
  currency: '$',
};
