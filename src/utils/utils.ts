import { Meal, MealWithQuantityIndex } from '../types/types';
import { config } from '../config';

const { currency } = config;

export const parseRawPrice = (price: number) =>
  `${currency}${price.toFixed(2)}`;

export const addSummaryItem = (
  item: Meal,
  summary: MealWithQuantityIndex,
  totalPrice: number
) => {
  const { id, price, name } = item;
  const newSummary = { ...summary };
  const newTotalPrice = totalPrice + price;
  if (newSummary[id]) {
    newSummary[id].selected += 1;
    newSummary[id].price += price;
  } else {
    newSummary[id] = {
      id,
      name,
      price,
      selected: 1,
    };
  }
  return {
    summary: newSummary,
    totalPrice: newTotalPrice,
  };
};

export const removeSummaryItem = (
  item: Meal,
  summary: MealWithQuantityIndex,
  totalPrice: number
) => {
  const { id, price } = item;
  const newTotalPrice = totalPrice - price;
  const newSummary = { ...summary };
  newSummary[id].selected -= 1;
  newSummary[id].price -= price;
  if (newSummary[id].selected === 0) {
    delete newSummary[id];
  }
  return {
    summary: newSummary,
    totalPrice: newTotalPrice,
  };
};
