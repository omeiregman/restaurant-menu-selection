import { addSummaryItem, parseRawPrice, removeSummaryItem } from './utils';

describe('price', () => {
  describe('parseRawPrice', () => {
    it('should return the raw price with the currency symbol and converted to decimals', () => {
      const price = 10;
      const parsedPrice = parseRawPrice(price);

      expect(parsedPrice).toBe('$10.00');
    });
  });
});

describe('AddSummaryItem', () => {
  it('adds a new item to the checkout summary', () => {
    const totalPrice = 6;
    const item = {
      id: 3,
      name: 'Fish',
      price: 3,
    };
    const summaryItem = {
      1: {
        id: 1,
        name: 'Soup',
        price: 3,
        selected: 1,
      },
      2: {
        id: 2,
        name: 'Bean',
        price: 5,
        selected: 1,
      },
    };
    const modified = {
      summary: {
        1: {
          id: 1,
          name: 'Soup',
          price: 3,
          selected: 1,
        },
        2: {
          id: 2,
          name: 'Bean',
          price: 5,
          selected: 1,
        },
        3: {
          id: 3,
          name: 'Fish',
          price: 3,
          selected: 1,
        },
      },
      totalPrice: 9,
    };
    expect(addSummaryItem(item, summaryItem, totalPrice)).toStrictEqual(
      modified
    );
  });
});

describe('RemoveSummaryItem', () => {
  it('removes an item from the checkout summary', () => {
    const totalPrice = 12;
    const item = {
      id: 2,
      name: 'Bean',
      price: 5,
    };
    const summaryItem = {
      1: {
        id: 1,
        name: 'Soup',
        price: 3,
        selected: 1,
      },
      2: {
        id: 2,
        name: 'Bean',
        price: 5,
        selected: 1,
      },
    };
    const modified = {
      summary: {
        1: {
          id: 1,
          name: 'Soup',
          price: 3,
          selected: 1,
        },
      },
      totalPrice: 7,
    };
    expect(removeSummaryItem(item, summaryItem, totalPrice)).toStrictEqual(
      modified
    );
  });
});
